import React from "react";
import './App.css';
import FilmTracker from "./containers/FilmTracker/FilmTracker";
import JokeMachine from "./containers/JokeMachine/JokeMachine";

function App() {
  return (
    <div className="App">
      <FilmTracker />
      <JokeMachine />
    </div>
  );
}

export default App;
