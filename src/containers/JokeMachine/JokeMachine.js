import React, {useEffect, useState} from 'react';
import Joke from "./Joke/Joke";

const JokeMachine = () => {
    const [jokes, setJokes] = useState([]);

    useEffect(() => {

        const jokeArray = [];

        const fetchData = async () => {
            const response = await fetch('https://api.chucknorris.io/jokes/random');

            if (response.ok) {
                const jokeArr = await response.json();
                jokeArray.push(jokeArr.value);
                // setJokes([...jokes, jokeArr.value]);
            }
        }

        for (let i = 0; i < 10; i++) {
            fetchData().catch(console.error);
        }
        setJokes([...jokeArray]);

    }, []);

    return (
        <div>
            {console.log(jokes)}
            {jokes.map(joke => {
                return <Joke jokeText={joke} />
            })}

        </div>
    );
};

export default JokeMachine;